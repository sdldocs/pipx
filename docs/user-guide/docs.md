# 문서 

```
pipx --help
usage: pipx [-h] [--version]
            {install,inject,upgrade,upgrade-all,uninstall,uninstall-all,reinstall,reinstall-all,list,run,runpip,ensurepath,environment,completions}
            ...

Install and execute apps from Python packages.

Binaries can either be installed globally into isolated Virtual Environments
or run directly in a temporary Virtual Environment.

Virtual Environment location is /opt/pipx/venvs.
Symlinks to apps are placed in /opt/pipx_bin.

optional environment variables:
  PIPX_HOME             Overrides default pipx location. Virtual Environments
                        will be installed to $PIPX_HOME/venvs.
  PIPX_BIN_DIR          Overrides location of app installations. Apps are
                        symlinked or copied here.
  PIPX_DEFAULT_PYTHON   Overrides default python used for commands.
  USE_EMOJI             Overrides emoji behavior. Default value varies based
                        on platform.

options:
  -h, --help            show this help message and exit
  --version             Print version and exit

subcommands:
  Get help for commands with pipx COMMAND --help

  {install,inject,upgrade,upgrade-all,uninstall,uninstall-all,reinstall,reinstall-all,list,run,runpip,ensurepath,environment,completions}
    install             Install a package
    inject              Install packages into an existing Virtual Environment
    upgrade             Upgrade a package
    upgrade-all         Upgrade all packages. Runs `pip install -U <pkgname>`
                        for each package.
    uninstall           Uninstall a package
    uninstall-all       Uninstall all packages
    reinstall           Reinstall a package
    reinstall-all       Reinstall all packages
    list                List installed packages
    run                 Download the latest version of a package to a
                        temporary virtual environment, then run an app from
                        it. Also compatible with local `__pypackages__`
                        directory (experimental).
    runpip              Run pip in an existing pipx-managed Virtual
                        Environment
    ensurepath          Ensure directories necessary for pipx operation are in
                        your PATH environment variable.
    environment         Print a list of variables used in pipx.constants.
    completions         Print instructions on enabling shell completions for
                        pipx
```

## `pipx install` <a id="pipx-install"></a>
```
pipx install --help
usage: pipx install [-h] [--include-deps] [--verbose] [--force]
                    [--suffix SUFFIX] [--python PYTHON]
                    [--system-site-packages] [--index-url INDEX_URL]
                    [--editable] [--pip-args PIP_ARGS]
                    package_spec

The install command is the preferred way to globally install apps
from python packages on your system. It creates an isolated virtual
environment for the package, then ensures the package's apps are
accessible on your $PATH.

The result: apps you can run from anywhere, located in packages
you can cleanly upgrade or uninstall. Guaranteed to not have
dependency version conflicts or interfere with your OS's python
packages. 'sudo' is not required to do this.

pipx install PACKAGE_NAME
pipx install --python PYTHON PACKAGE_NAME
pipx install VCS_URL
pipx install ./LOCAL_PATH
pipx install ZIP_FILE
pipx install TAR_GZ_FILE

The PACKAGE_SPEC argument is passed directly to `pip install`.

The default virtual environment location is ~/.local/pipx
and can be overridden by setting the environment variable `PIPX_HOME`
(Virtual Environments will be installed to `$PIPX_HOME/venvs`).

The default app location is ~/.local/bin and can be
overridden by setting the environment variable `PIPX_BIN_DIR`.

The default python executable used to install a package is
typically the python used to execute pipx and can be overridden
by setting the environment variable `PIPX_DEFAULT_PYTHON`.

positional arguments:
  package_spec          package name or pip installation spec

options:
  -h, --help            show this help message and exit
  --include-deps        Include apps of dependent packages
  --verbose
  --force, -f           Modify existing virtual environment and files in
                        PIPX_BIN_DIR
  --suffix SUFFIX       Optional suffix for virtual environment and executable
                        names. NOTE: The suffix feature is experimental and
                        subject to change.
  --python PYTHON       The Python executable used to create the Virtual
                        Environment and run the associated app/apps. Must be
                        v3.6+.
  --system-site-packages
                        Give the virtual environment access to the system
                        site-packages dir.
  --index-url INDEX_URL, -i INDEX_URL
                        Base URL of Python Package Index
  --editable, -e        Install a project in editable mode
  --pip-args PIP_ARGS   Arbitrary pip arguments to pass directly to pip
                        install/upgrade commands
```

## `pipx run` <a id="pipx-run"></a>
```
pipx run --help
usage: pipx run [-h] [--no-cache] [--pypackages] [--spec SPEC] [--verbose]
                [--python PYTHON] [--system-site-packages]
                [--index-url INDEX_URL] [--editable] [--pip-args PIP_ARGS]
                app ...

Download the latest version of a package to a temporary virtual environment,
then run an app from it. The environment will be cached
and re-used for up to 14 days. This
means subsequent calls to 'run' for the same package will be faster
since they can re-use the cached Virtual Environment.

In support of PEP 582 'run' will use apps found in a local __pypackages__
directory, if present. Please note that this behavior is experimental,
and acts as a companion tool to pythonloc. It may be modified or
removed in the future. See https://github.com/cs01/pythonloc.

positional arguments:
  app ...               app/package name and any arguments to be passed to it

options:
  -h, --help            show this help message and exit
  --no-cache            Do not re-use cached virtual environment if it exists
  --pypackages          Require app to be run from local __pypackages__
                        directory
  --spec SPEC           The package name or specific installation source
                        passed to pip. Runs `pip install -U SPEC`. For example
                        `--spec mypackage==2.0.0` or `--spec
                        git+https://github.com/user/repo.git@branch`
  --verbose
  --python PYTHON       The Python version to run package's CLI app with. Must
                        be v3.6+.
  --system-site-packages
                        Give the virtual environment access to the system
                        site-packages dir.
  --index-url INDEX_URL, -i INDEX_URL
                        Base URL of Python Package Index
  --editable, -e        Install a project in editable mode
  --pip-args PIP_ARGS   Arbitrary pip arguments to pass directly to pip
                        install/upgrade commands
```

## `pipx upgrade` <a id="pipx-upgrade"></a>
```
pipx upgrade --help
usage: pipx upgrade [-h] [--include-injected] [--force]
                    [--system-site-packages] [--index-url INDEX_URL]
                    [--editable] [--pip-args PIP_ARGS] [--verbose]
                    package

Upgrade a package in a pipx-managed Virtual Environment by running 'pip
install --upgrade PACKAGE'

positional arguments:
  package

options:
  -h, --help            show this help message and exit
  --include-injected    Also upgrade packages injected into the main app's
                        environment
  --force, -f           Modify existing virtual environment and files in
                        PIPX_BIN_DIR
  --system-site-packages
                        Give the virtual environment access to the system
                        site-packages dir.
  --index-url INDEX_URL, -i INDEX_URL
                        Base URL of Python Package Index
  --editable, -e        Install a project in editable mode
  --pip-args PIP_ARGS   Arbitrary pip arguments to pass directly to pip
                        install/upgrade commands
  --verbose
```

## `pipx upgrade-all` <a id="pipx-upgrade-all"></a>
```
pipx upgrade-all --help
usage: pipx upgrade-all [-h] [--include-injected] [--skip SKIP [SKIP ...]]
                        [--force] [--verbose]

Upgrades all packages within their virtual environments by running 'pip
install --upgrade PACKAGE'

options:
  -h, --help            show this help message and exit
  --include-injected    Also upgrade packages injected into the main app's
                        environment
  --skip SKIP [SKIP ...]
                        skip these packages
  --force, -f           Modify existing virtual environment and files in
                        PIPX_BIN_DIR
  --verbose
```

## `pipx inject` <a id="pipx-inject"></a>
```
pipx inject --help
usage: pipx inject [-h] [--include-apps] [--include-deps]
                   [--system-site-packages] [--index-url INDEX_URL]
                   [--editable] [--pip-args PIP_ARGS] [--force] [--verbose]
                   package dependencies [dependencies ...]

Installs packages to an existing pipx-managed virtual environment.

positional arguments:
  package               Name of the existing pipx-managed Virtual Environment
                        to inject into
  dependencies          the packages to inject into the Virtual Environment--
                        either package name or pip package spec

options:
  -h, --help            show this help message and exit
  --include-apps        Add apps from the injected packages onto your PATH
  --include-deps        Include apps of dependent packages
  --system-site-packages
                        Give the virtual environment access to the system
                        site-packages dir.
  --index-url INDEX_URL, -i INDEX_URL
                        Base URL of Python Package Index
  --editable, -e        Install a project in editable mode
  --pip-args PIP_ARGS   Arbitrary pip arguments to pass directly to pip
                        install/upgrade commands
  --force, -f           Modify existing virtual environment and files in
                        PIPX_BIN_DIR
  --verbose
```

## `pipx uninstall` <a id="pipx-uninstall"></a>
```
pipx uninstall --help
usage: pipx uninstall [-h] [--verbose] package

Uninstalls a pipx-managed Virtual Environment by deleting it and any files
that point to its apps.

positional arguments:
  package

options:
  -h, --help  show this help message and exit
  --verbose
```

## `pipx unintsall-all` <a id="pipx-uninstall-all"></a>
```
pipx uninstall-all --help
usage: pipx uninstall-all [-h] [--verbose]

Uninstall all pipx-managed packages

options:
  -h, --help  show this help message and exit
  --verbose
```

## `pipx reinstall-all` <a id="pipx-reinstall-all"></a>
```
pipx reinstall-all --help
usage: pipx reinstall-all [-h] [--python PYTHON] [--skip SKIP [SKIP ...]]
                          [--verbose]

Reinstalls all packages.

Packages are uninstalled, then installed with pipx install PACKAGE
with the same options used in the original install of PACKAGE.
This is useful if you upgraded to a new version of Python and want
all your packages to use the latest as well.

options:
  -h, --help            show this help message and exit
  --python PYTHON       The Python executable used to recreate the Virtual
                        Environment and run the associated app/apps. Must be
                        v3.6+.
  --skip SKIP [SKIP ...]
                        skip these packages
  --verbose
```

## `pipx list` <a id="pipx list"></a>
```
pipx reinstall-all --help
usage: pipx reinstall-all [-h] [--python PYTHON] [--skip SKIP [SKIP ...]]
                          [--verbose]

Reinstalls all packages.

Packages are uninstalled, then installed with pipx install PACKAGE
with the same options used in the original install of PACKAGE.
This is useful if you upgraded to a new version of Python and want
all your packages to use the latest as well.

options:
  -h, --help            show this help message and exit
  --python PYTHON       The Python executable used to recreate the Virtual
                        Environment and run the associated app/apps. Must be
                        v3.6+.
  --skip SKIP [SKIP ...]
                        skip these packages
  --verbose
```

## `pipx runpip` <a id="pipx-runpip"></a>
```
pipx runpip --help
usage: pipx runpip [-h] [--verbose] package ...

Run pip in an existing pipx-managed Virtual Environment

positional arguments:
  package     Name of the existing pipx-managed Virtual Environment to run pip
              in
  pipargs     Arguments to forward to pip command

options:
  -h, --help  show this help message and exit
  --verbose
```
