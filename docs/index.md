<p align="center">
<img align="center" src="https://github.com/pypa/pipx/raw/main/logo.png" width="200"/>
</a>
</p>

# pipx — 격리된 환경에서 Python 응용의 설치와 실행 (Install and Run Python Applications in Isolated Environments)

## pipx 설치 <a id="install-pipx"></a>
### MacOS
```shell
brew install pipx
pipx ensurepath
```

`brew update && brew upgrade pipx` 명령으로 pipx를 업그레이드할 수 있다.

### Linux, pip으로 설치 (pip 19.0 이상이 필요)
```shell
python3 -m pip install --user pipx
python3 -m pipx ensurepath
```

`python3 -m pip install --user -U pipx` 명령으로 pipx를 업그레이드할 수 있다.

### Windows, pip으로 설치 (pip 19.0 이상이 필요)
```
# If you installed python using the app-store, replace `python` with `python3` in the next line.
python -m pip install --user pipx
```

위의 수행이 다음과 같은 WARNNING으로 (대부분) 끝날 수 있다. 
```
WARNING: The script pipx.exe is installed in `<USER folder>\AppData\Roaming\Python\Python3x\Scripts` which is not on PATH
```

이 경우 pipx 실행 파일을 직접 실행할 수 있도록 위의 폴더로 이동시킨다. 경고를 받지 않은 경우에도 다음 행을 입력란다.
```
pipx ensurepath
```

그러면 위의 경로와 `%USERPROFILE%local\bin` 폴더가 검색 경로에 추가된다. 터미널 세션을 재시작하고 `pipx`을 실행할 수 있는지 확인한다.

`python3 -m pip install --user -U pipx` 명령으로 pipx를 업그레이드할 수 있다.

### 쉘 완료 <a id="shell-completion"></a>
셸 완료는 다음 명령어로 출력된 절차에 따라 수행할 수 있다.
```shell
pipx completions
```
보다 자세한 사항을 위하여는 [installation instructions](https://pypa.github.io/pipx/installation/)을 살펴보도록 한다.

## 개요: `pipx`란? <a id="overview"></a>
pipx는 Python으로 작성된 최종 사용자 애플리케이션을 설치하고 실행하는 데 도움이 되는 도구이다. 이는 macOS의 `brew`, `JavaScript의 npx`, Linux의 `apt`와 거의 유사하다.

pip과 밀접한 관련이 있다. 실제로는 pip을 사용하지만 명령에서 애플리케이션으로 직접 실행할 수 있는 Python 패키지를 설치하고 관리하는 데 중점을 두고 있다.

### pip과 다른 점 <a id="how-is-it-different-from-pip"></a>
pip은 라이브러리와 애플리케이션 모두에 대한 범용 패키지 설치 프로그램이며, 환경을 분리하지 않는다. pipx는 분리를 추가하면서도 쉘에서 애플리케이션을 사용할 수 있도록 하기 위해 애플리케이션 설치용으로 특별히 제작되었다.pipx는 각 애플리케이션과 관련 패키지를 위한 격리된 환경을 만든다.

pipx에는 pip이 포함되어 있지 않지만, 설치할 때 시스템 부트스트래핑의 중요한 부분이 되가도 헌다.

### 'pipx'는 어디로부터 앱을 설치 <a id="where-does-pipx-install-apps-from"></a>
기본적으로는 pipx는 pip과 동일한 패키지 인덱스를 사용한다. [PyPI](https://pypi.org/).pipx는 pip 처럼 로컬 디렉토리, 휠, git url 등과 같은 다른 모든 소스 에서 설치할 수도 있다.

Python과 PyPI를 통해 개발자는 "콘솔 스크립트 엔트리 포인트"를 갖는 코드를 배포할 수 있다. 이러한 엔트리 포인트를 통해 사용자는 명령어로 Python 코드를 호출하여 독립적으로 실행할 수 있는 애플리케이션처럼 효과적으로 작동시틸 수 있다.

pipx는 수천 개의 이같은 응용 프로그램 패키지를 안전하고 편리하며 신뢰할 수 있는 방법으로 설치하고 실행할 수 있도록 지원하는 도구이다. **어떤 면에서 Python Package Index(PyPI)를 Python 어플리케이션용 큰 앱스토어로 만든다**. 모든 Python 패키지에 엔트리 포인트가 있는 것은 아니지만 많은 패키지에 있다.

패키지가 pipx와 호환되도록 하려면 [콘솔 스크립트](https://python-packaging.readthedocs.io/en/latest/command-line-scripts.html#the-console-scripts-entry-point) 엔트리 포인트만 추가하면 된다. 사용자는 [다음 지침](https://python-poetry.org/docs/pyproject/#scripts)을 사용하도록 한다.

## 기능 <a id="features"></a>
`pipx`는

- 패키지("앱(apps)")의 CLI 엔트리 포인트를 `install` 명령어를 사용하여 설치한 격리된 환경에 표시한다. 이렇게 하면 종속성 충돌 회피와 클린 제거를 보장한다.
- pipx로 설치된 패키지를 쉽게 나열, 업그레이드 및 제거할 수 있다.
- `run` 명령으로 임시 환경에서 최신 버전의 Python 응용 프로그램을 실행한다.

무엇보다도 pipx는 일반 사용자 권한으로 실행되며 `sudo pip install`을 호출하지 않는다 (그렇게 하지 안는다. 그렇지요? 😄).

### pipx로 패키지와 어플리케이션 설치 <a id="installing-a-package-and-its-applications-with-pipx"></a>
애플리케이션을 글로벌하게 설치하려면 다음을 실행한다.
```
pipx install PACKAGE
```

그러면 가상 환경이 자동으로 생성되고, 패키지가 설치되며, 패키지에 연결된 애플리케이션(엔트리 포인트)이 PATH의 위치에 추가된다. 예를 들어 `pipx install pycowsay`는 `pycowsay`를 `pycowsay` 명령으로 글로벌하게 실행할 수 있도록 하지만 pycowsay 패키지를 자체 가상 환경으로 샌드박스화 한다. **이를 위해 pipx를 sudo로 실행할 필요는 없다**.

예:
```
>> pipx install pycowsay
  installed package pycowsay 2.0.3, Python 3.7.3
  These apps are now globally available
    - pycowsay
done! ✨ 🌟 ✨


>> pipx list
venvs are in /home/user/.local/pipx/venvs
apps are exposed on your $PATH at /home/user/.local/bin
   package pycowsay 2.0.3, Python 3.7.3
    - pycowsay


# Now you can run pycowsay from anywhere
>> pycowsay mooo
  ____
< mooo >
  ====
         \
          \
            ^__^
            (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```



### 소스 관리 도구로부터 설치 <a id="installing-from-source-control"></a>

### 임시 가상 환경에서 애플리케이션 실행 <a id="running-an-application-in-a-temporary-virtual-environment"></a>

### 소스 관리 도구로부터 실행 <a id="running-from-source-control"></a>

### 요약 <a id="summary"></a>
That's it! 이들이 `pipx`가 제공하는 가장 중요한 명령어이다. pipx의 모든 문서를 보려면 `pipx --help`를 실행하거나 [문서](user-guide/docs.md)를 참조하도록 한다.

